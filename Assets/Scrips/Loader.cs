﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Loader : MonoBehaviour {

    bool isModelRotation = false;
    bool isUsingEnvironment = false;
    bool isBloomActive = true;

    public GUISkin GuiSkin;
    public Camera mainCamera;
    public GameObject environment;
    public GameObject testPlane;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
	    if (isModelRotation)
        {
            this.transform.Rotate(new Vector3(0, 1, 0), 10f * Time.deltaTime);
        }
	}

     void OnGUI()
    {
        GUI.skin = GuiSkin;
        Mesh[] meshes = Resources.LoadAll<Mesh>("Smoothed Meshes");

        for (int i = 0; i < meshes.Length; i++)
        {
            if ( GUI.Button(new Rect(10, 10 + i * 22, 200, 20), new GUIContent(meshes[i].name)) )
            {
                gameObject.GetComponent<MeshFilter>().mesh = meshes[i];
                Texture[] textures = Resources.LoadAll<Texture>("");
                for (int j = 0; j < textures.Length; j++)
                {
                    if (textures[j].name == meshes[i].name)
                    {
                        GetComponent<MeshRenderer>().material.SetTexture("_MainTex", textures[j]);
                        break;
                    }
                }
                for (int j = 0; j < textures.Length; j++)
                {
                    if (textures[j].name == meshes[i].name + "_normal")
                    {
                        GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", textures[j]);
                        break;
                    }
                }
            }
        }

        if ( GUI.Toggle(new Rect(220, 10, 200, 20), isModelRotation,"Model Rotation")  != isModelRotation)
        {
            isModelRotation = !isModelRotation;
        }

        if (GUI.Toggle(new Rect(220, 26, 200, 20), isUsingEnvironment, "Using environment") != isUsingEnvironment)
        {
            isUsingEnvironment = !isUsingEnvironment;

            testPlane.SetActive(!isUsingEnvironment);
            environment.SetActive(isUsingEnvironment);
        }

        if (GUI.Toggle(new Rect(220, 42, 200, 20), isBloomActive, "Bloom") != isBloomActive)
        {
            isBloomActive = !isBloomActive;
            mainCamera.GetComponent<Bloom>().enabled = isBloomActive;
        }
    }
}
