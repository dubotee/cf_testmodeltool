== How to import FBX
- Copy your FBX into Assets/Resources folder
- Copy your texture into Assets/Resources folder
- Rename your diffuse texture file name the same with model name.
- Rename your normal (bump) texture file name like this: [filename]_normal

== Using Unity
- Open this Unity project
- Open menu "FacerollGames/Generate Tangent Meshes" and wait for the code is doing its job
- Open Test scene
- Press Play to use
 